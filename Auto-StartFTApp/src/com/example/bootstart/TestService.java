package com.example.bootstart;

import java.util.List;

import android.app.ActivityManager;
import android.app.Service;
import android.app.ActivityManager.RunningAppProcessInfo;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Handler;
import android.os.IBinder;
import android.widget.Toast;

public class TestService extends Service {
	public TestService() {
	}

	@Override
	public IBinder onBind(Intent intent) {
		// TODO: Return the communication channel to the service.
		throw new UnsupportedOperationException("Not yet implemented");
	}
	@Override
	public void onCreate() {
		// TODO Auto-generated method stub
	//Toast.makeText(getApplicationContext(), "Service Created",1).show();
		super.onCreate();
	}
	
	
	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		//Toast.makeText(getApplicationContext(), "Service Destroy",1).show();
		super.onDestroy();
	}
	
	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		// TODO Auto-generated method stub
		//Toast.makeText(getApplicationContext(), "Service Working",1).show();
		repeatAtFixedRate();
		return super.onStartCommand(intent, flags, startId);
	}
	
	
	private void repeatAtFixedRate(){
		new Handler().postDelayed(new Thread(){
			@Override
			public void run() {
				runDemon();	
				repeatAtFixedRate();
			}
		
		},30*1000 );
		
	}
	
	private void runDemon(){
		
			if (isProcessRunning("com.foottraficker")) {
				//Toast.makeText(this, "ReLaunching FT app", Toast.LENGTH_LONG)
					//	.show();
				Intent i = new Intent();
				i.setComponent(new ComponentName("com.foottraficker",
						"com.foottraficker.FootTrafficker"));
				i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				
				this.startActivity(i);
			} else {
				//Toast.makeText(this, "Starting App", Toast.LENGTH_LONG).show();
				Intent i = new Intent();
				i.setComponent(new ComponentName("com.foottraficker",
						"com.foottraficker.FootTrafficker"));
				i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				
				this.startActivity(i);
			}
			
	}
	
	private boolean isProcessRunning(String process) {
		ActivityManager activityManager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
		List<RunningAppProcessInfo> procInfos = activityManager
				.getRunningAppProcesses();
		for (int i = 0; i < procInfos.size(); i++) {
			if (procInfos.get(i).processName.equals(process)) {

				return true;
			}

		}
		return false;

	}
}
